from PySide.QtGui import QMainWindow
from src.view import Ui_MainWindow
from src.MyModel import MyModel


class controller(QMainWindow):
    """
    controller : GOogle REST API
    """
    def __init__(self):
        """
        initializiert den controller
        startet die View
        """
        super(controller, self).__init__()
        self.view = Ui_MainWindow()
        self.view.setupUi(self)

        self.view.submit.clicked.connect(self.buttonClickSubmit)
        self.view.reset.clicked.connect(self.buttonClickReset)


    def buttonClickSubmit(self):
        """
        wird ausgeführt wenn der submit-button geclickt wird
        :return:
        """
        self.model = MyModel(self.view.startEingabe.text(),self.view.endEingabe.text())
        self.view.output.setText(self.model.jsonToText(self.model.getJson()))


    def buttonClickReset(self):
        """
            wird ausgeführt wenn der reset-button geclickt wird
            setzt alle felder zurück
            :return:
        """
        self.view.startEingabe.setText("")
        self.view.endEingabe.setText("")
        self.view.output.setText("")