import argparse
from http.server import HTTPServer, BaseHTTPRequestHandler
from spacy_cld import LanguageDetector

class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response("JAshdkjashdkjashd")
        nlp = spacy.load('en')
        language_detector = LanguageDetector()
        nlp.add_pipe(language_detector)
        doc = nlp('This is some English text.')
        doc._.languages



def run(address,port,server_class=HTTPServer, handler_class=MyHandler):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--address', metavar='address', type=str,
                        help='address',default="localhost")
    parser.add_argument('--port', metavar='address', type=int,
                        help='port',default=8081)
    args = parser.parse_args()
    run(args.address,args.port)