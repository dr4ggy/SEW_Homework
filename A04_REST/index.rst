.. rest api probematura documentation master file, created by
   sphinx-quickstart on Wed Dec 20 09:31:08 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to rest api probematura's documentation!
================================================

Contents:
---------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   model
   controller



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
