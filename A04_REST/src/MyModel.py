import json
import requests

class MyModel():
    """
    Model für Google REST API
    Schickt Anfragen an google und erhält responses
    """
    def __init__(self, origin,dest):
        """
        Initialisiert das Model
        Origin udn Destination werden gesetzt
        :param origin: start location
        :param dest: destination
        """
        self.url = "http://maps.googleapis.com/maps/api/directions/json"
        self.origin = origin
        self.dest = dest



    def getJson(self):
        """
        sendet die anfrage und erhält response
        :return: json to return
        """
        params = {"origin": self.origin,
                  "destination" : self.dest,
                  "sensor" : "false",
                  "language" : "de",
        }
        print(params)
        try:
            json_data = requests.get(self.url,params) #anfrage wird geschickt und empfangen
        except:
            return "Connection Error"

        return json_data.json()

    def jsonToText(self,jsonD):
        """
        bringt das von getJson() erhaltene Format in eine lesbare form
        entfernt unbenutzte information udn gibt diese als String zurücl
        :param jsonD: das rohe json
        :return: String mit relevanter information
        """

        origin = self.origin
        dest = self.dest

        print(jsonD)
        if(jsonD['status'] == "ZERO_RESULTS"):
            return 'herr Professor...Sie müssen schon existierende Orte bei start und ziel eingeben.<br><b>\"' + origin + '\"</b> und <b>\'"' + dest + '\"</b> kenne ich nicht und Sie auch nicht'

        if(jsonD['status'] == "NOT_FOUND"):
            self.origin = dest
            checkdest = self.getJson()
            self.dest = origin
            self.origin = origin
            checkorigin = self.getJson()
            if(checkdest['status'] == "NOT_FOUND" and checkorigin['status'] == "NOT_FOUND" ) :
                return 'herr Professor...Sie müssen schon existierende Orte bei start und ziel eingeben.<br><b>\"'+origin+'\"</b> und <b>\'"'+dest+'\"</b> kenne ich nicht und Sie auch nicht'
            if(checkorigin['status'] == "NOT_FOUND") :
                return 'herr Professor...Sie müssen schon einen existierenden Ort bei start eingeben.<br><b>\"'+origin+'\"</b> kenne ich nicht und Sie auch nicht'
            if(checkdest['status'] == "NOT_FOUND"):
                return 'herr Professor...Sie müssen schon einen existierenden Ort bei ziel eingeben.<br><b>\"'+dest+'\"</b> kenne ich nicht und Sie auch nicht '
        else:
            self.origin = origin
            self.dest = dest
            str = ""
            str += "<b>Gesammtdistanz : "+jsonD['routes'][0]['legs'][0]['distance']['text'] + "<br>"
            str += "Gesammtdauer : "+jsonD['routes'][0]['legs'][0]['duration']['text'] + "</b><br><br>"
            print (jsonD)
            for i in jsonD['routes'][0]['legs'][0]['steps'] :
                str += i['html_instructions'] + "<br>"
            return str