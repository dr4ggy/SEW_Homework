from PySide.QtGui import QMainWindow
from src.view import Ui_MainWindow
from src.MyModel import MyModel


class controller(QMainWindow):
    """
    controller : GOogle REST API
    """
    def __init__(self):
        """
        initializiert den controller


        startet die View
        """
        super(controller, self).__init__()
        self.view = Ui_MainWindow()
        self.view.setupUi(self)

        self.view.submit.clicked.connect(self.buttonClickSubmit)
        self.view.reset.clicked.connect(self.buttonClickReset)


    def buttonClickSubmit(self):
        """
        wird ausgeführt wenn der submit-button geclickt wird
        :return:
        """
        self.view.output.setText("")

        start = self.view.startEingabe.text()
        ziel = self.view.endEingabe.text()
        if(start == "" and ziel == ""):
            self.view.output.setText("Herr professor .. sie müssen schon was bei 'start' und bei 'ziel' eingeben")
        elif(ziel == ""):
            self.view.output.setText("Herr professor .. sie müssen schon was bei 'ziel' eingeben")
        elif(start == ""):
            self.view.output.setText("Herr professor .. sie müssen schon was bei 'start' eingeben")
        else:
            self.model = MyModel(self.view.startEingabe.text(),self.view.endEingabe.text())
            try:
                self.view.output.setText(self.model.jsonToText(self.model.getJson()))
            except:
                self.view.output.setText("There is no Internetz! Try it outside of ze TGM")

    def buttonClickReset(self):
        """
            wird ausgeführt wenn der reset-button geclickt wird
            setzt alle felder zurück
            :return:
        """
        self.view.startEingabe.setText("")
        self.view.endEingabe.setText("")
        self.view.output.setText("")