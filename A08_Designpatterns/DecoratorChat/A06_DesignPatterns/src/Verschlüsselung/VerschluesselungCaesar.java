package Verschlüsselung;
import java.io.IOException;

import ChannelInterface.IChannel;

public class VerschluesselungCaesar extends ChannelDecorator{
	int counter = 1;
	
	public VerschluesselungCaesar(IChannel wrapper) {
		super(wrapper);
	}
	
	/**
	 * Encrypts message as String and calls super.printline()
	 */
	@Override
	public void printLine(String message) throws IOException {
		char[] newmessage = message.toCharArray();
		for(int i = 0 ; i < newmessage.length;i++) {
			newmessage[i] = (char) (newmessage[i]+this.counter);
		}
		super.printLine(String.copyValueOf(newmessage));
	}

	/**
	 * Encrypts message as bytestream and calls super.printline()
	 */
	@Override
	public void printLine(byte[] message) throws IOException {
		this.printLine(new String(message));
	}

	/**
	 * Decrypts the message as String
	 */
	@Override
	public String readLine() throws IOException {
		char[] newmessage = super.readLine().toCharArray();
		for(int i = 0 ; i < newmessage.length;i++) {
			newmessage[i] = (char) (newmessage[i]-this.counter);
		}
		return String.copyValueOf(newmessage);
	}
	
	/**
	 * Decrypts the message as bytestream
	 */
	@Override
	public byte[] readLineBytes() throws IOException {
		return this.readLine().getBytes();
	}


	@Override
	public void close() throws IOException {
		this.wrapper.close();
	}

	

}
