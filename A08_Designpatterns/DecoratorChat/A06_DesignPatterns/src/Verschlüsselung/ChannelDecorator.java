package Verschlüsselung;
import java.io.IOException;

import ChannelInterface.IChannel;

/**
 * Channel Decorator
 * Basis class for every Concrete ChannelDecorator.
 * The Method of Each wrapper is calles in each Method
 * @author Draggy
 *
 */
public  class ChannelDecorator implements IChannel{
	/**
	 * IChannel wrapper to be decorated
	 */
	protected IChannel wrapper;
	
	public ChannelDecorator(IChannel wrapper) {
		this.wrapper = wrapper;
	}

	@Override
	public void printLine(String message) throws IOException {
		wrapper.printLine(message);
	}

	@Override
	public void printLine(byte[] message) throws IOException {
		wrapper.printLine(message);
	}

	@Override
	public String readLine() throws IOException {
		return wrapper.readLine();
	}

	@Override
	public byte[] readLineBytes() throws IOException {
		return wrapper.readLineBytes();
	}

	@Override
	public void close() throws IOException {
		this.wrapper.close();
	}


}
