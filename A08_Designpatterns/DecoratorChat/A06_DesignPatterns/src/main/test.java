package main;

import java.io.IOException;

import Client.Client;
import Server.Server;
import Verschlüsselung.VerschluesselungAES;
import Verschlüsselung.VerschluesselungCaesar;
/**
 * Test der Client Server connection
 * @author Draggy
 *
 */
public class test {
	public static void main(String[] args) {
		try {
			int port = 9140;



			Server s = new Server(port);
			Client c = new Client(port);
			s.accept();
			
			VerschluesselungCaesar vcs = new VerschluesselungCaesar(s);
			VerschluesselungCaesar vcc = new VerschluesselungCaesar(c);
			

			VerschluesselungAES vcs2 = new VerschluesselungAES(vcs);
			VerschluesselungAES vcc2 = new VerschluesselungAES(vcc);
			
			vcc2.printLine("Hallo");
			System.out.println(vcs2.readLine());
			System.out.println("---------------");
			vcs2.printLine("hallo2");
			System.out.println(vcc2.readLine());
			System.out.println("---------------");
			
			c.close();
			s.close();
		} catch (IOException e) {
			System.out.println("Error");
		}
	} 
}
