package Server;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import ChannelInterface.IChannel;
/**
 * The Server for the Client-Server communication
 * Implements the IChannel Interface in order to 
 * be able to Decorate is with a ChannelDecorator
 * @author Draggy
 *
 */
public class Server implements IChannel{
	private ServerSocket socket;
	Socket listener;
	public Server(int port) {
		try {
			this.socket  = new ServerSocket(port);
		} catch (Exception e) {
			System.err.print("Error creating Socket on Server");
		} 
	}
	
	/**
	 * Senden der Message als String
	 */
	public void printLine(String message) throws IOException {
		PrintWriter out = new PrintWriter(listener.getOutputStream(), true);
		out.flush();
        out.println(message);
	}
	
	/**
	 * Senden der Message als byte-Stream
	 */
	public void printLine(byte[] message) throws IOException {
		PrintWriter out = new PrintWriter(listener.getOutputStream(), true);
		out.flush();
        out.println(message);
	}
	
	/**
	 * Empfangen der Message als String
	 */
	public String readLine() throws IOException {
		BufferedReader input  = new BufferedReader(new InputStreamReader(listener.getInputStream()));
	    return input.readLine();
	}
	
	/**
	 * Empfangen der Message als String
	 */
	public byte[] readLineBytes() throws IOException {
		BufferedReader input  = new BufferedReader(new InputStreamReader(listener.getInputStream()));
	    return input.readLine().getBytes();
	}
	
	/**
	 * Schlie�en des Socktes
	 */
	public void close() throws IOException {
		this.socket.close();
	}
	
	/**
	 * Akzeptieren der Socket verbindung
	 */
	public void accept() {
		try {
			listener = this.socket.accept();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
