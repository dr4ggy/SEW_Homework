package ChannelInterface;


import java.io.Closeable;
import java.io.IOException;
/**
 * IChannel Interface 
 * 
 * @author Draggy
 *
 */
public interface IChannel extends Closeable {
	
	public void printLine(String message) throws IOException;

	public void printLine(byte[] message) throws IOException;

	public String readLine() throws IOException;

	public byte[] readLineBytes() throws IOException;

	@Override
	public void close() throws IOException;
}
