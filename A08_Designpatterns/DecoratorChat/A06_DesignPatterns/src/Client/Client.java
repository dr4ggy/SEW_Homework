package Client;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import ChannelInterface.IChannel;
/**
 * The Client for the Client-Server communication
 * Implements the IChannel Interface in order to 
 * be able to Decorate is with a ChannelDecorator
 * @author Draggy
 *
 */
public class Client implements IChannel{
	public Socket socket;	//Socket f�r kommunikation mit Server
	
	public Client(int port) {
		try {
			this.socket = new Socket("localhost", port);
		} catch (IOException e) {
			System.err.print("Error creating Socket on Client");
		}
	}
	
	/**
	 * Senden der Message als String
	 */
	public void printLine(String message) throws IOException {
		PrintWriter out = new PrintWriter(this.socket.getOutputStream(),true);
		out.flush();
		out.println(message);
	}

	/**
	 * Sender der Message als Bytestream
	 */
	public void printLine(byte[] message) throws IOException {
		PrintWriter out = new PrintWriter(this.socket.getOutputStream(),true);
		out.flush();
		out.println(message);
	}

	/**
	 * Lesen der erhaltenen Message als String
	 */
	public String readLine() throws IOException {
		InputStreamReader i = new InputStreamReader(this.socket.getInputStream());
		BufferedReader br = new BufferedReader(i);
		return br.readLine();
	}

	/**
	 *  Lesen der erhaltenen Message als Bytestream
	 */
	public byte[] readLineBytes() throws IOException {
		InputStreamReader i = new InputStreamReader(this.socket.getInputStream());
		BufferedReader br = new BufferedReader(i);
		return br.readLine().getBytes();
	}

	/**
	 * Schlie�en des Sockets
	 */
	public void close() throws IOException {
		this.socket.close();
	}

}
