\select@language {ngerman}
\contentsline {section}{\numberline {1}Einf\IeC {\"u}hrung}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Funktionalit\IeC {\"a}t (20 P.)}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Dokumentation (20 P.)}{1}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Protokoll (30 P.)}{1}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Erweiterung (30 P.)}{2}{subsection.1.4}
\contentsline {section}{\numberline {2}Beschreibung der Aufgabe}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Klassendiagramm}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Verbale Beschreibung}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Design Patterns}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Wie k\IeC {\"o}nnen Design Patterns unterteilt werden}{4}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}creational patterns}{4}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}structural patterns}{5}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}behavior patterns}{5}{subsubsection.3.1.3}
\contentsline {subsection}{\numberline {3.2}Wozu Design Patterns}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}\IeC {\"U}bersicht existierender Design Patterns}{6}{subsection.3.3}
\contentsline {paragraph}{Observer}{6}{section*.5}
\contentsline {paragraph}{Decorator}{6}{section*.7}
\contentsline {paragraph}{Factory}{6}{section*.9}
\contentsline {paragraph}{Strategy}{7}{section*.11}
\contentsline {section}{\numberline {4}Decorator Pattern}{8}{section.4}
\contentsline {subsection}{\numberline {4.1}Allgemeines Klassendiagramm}{8}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Grundz\IeC {\"u}ge des Design Patterns}{8}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Vor- und Nachteile}{8}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Weitere Anwendungsf\IeC {\"a}lle}{9}{subsection.4.4}
\contentsline {section}{\numberline {5}Strategy Pattern}{10}{section.5}
\contentsline {subsection}{\numberline {5.1}Allgemeines Klassendiagramm}{10}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Grundz\IeC {\"u}ge des Design Patterns}{10}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Vor- und Nachteile}{10}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Weitere Anwendungsf\IeC {\"a}lle}{11}{subsection.5.4}
\contentsline {section}{\numberline {6}resources}{12}{section.6}
