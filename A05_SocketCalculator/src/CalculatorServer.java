import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.ArrayList;

/**
 * Created by Draggy on 19.01.2018.
 */
public class CalculatorServer {
    private String ip;
    private int port;
    private int x; // connection
    private ServerSocket socket;
    private ArrayList<Connection> connection = new ArrayList<Connection>();

    public CalculatorServer(String ip, int port) {
        this.ip = ip;
        this.port = port;
        try {
            this.socket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void waitingForConnections(){
        while (true) {
            try {
                Socket clientSocket = this.socket.accept();
                PrintWriter out =
                        new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));
                connection.add(new Connection(out, in));
                connection.get(connection.size()-1).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    public static void main(String []args){
        CalculatorServer cs = new CalculatorServer("127.0.0.1", 12345);
        cs.waitingForConnections();
    }
}

