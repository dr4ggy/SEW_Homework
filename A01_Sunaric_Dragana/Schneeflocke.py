import threading
import time
import random


class Schneeflocke(threading.Thread):
    """
    Die Klasse Shcneeflocke erbt von threading.Thread
    Hier werden vom benutzer eingegebene Zahlen und Befehle entgegengenommen
    """
    max_y = 40
    stop = 0
    lock = threading.Lock()
    def __init__(self,posx,event):
        """
        Erstellt eine wait_for_input instanz
        :param queue: Die queue die von wait_for_input und von Consumer2 benutzt wird
        """
        self.event = event
        self.possition_y = 0
        self.possition_x = posx

        threading.Thread.__init__(self)

    def run(self):

        """
        Hier passiert das was der Thread tatsaechlich ausfuehren soll.
        Es werden Schneeflocken am fly been sein
        :return:
        """
        self.event.wait()
        while True:
            time.sleep(random.random())
            if self.possition_y == Schneeflocke.max_y:
                break
            time.sleep(random.random())
            if random.random()<0.5 :
                self.possition_x += 1
            if random.random()>=0.5:
                self.possition_x -= 1
            self.possition_y += 1
        with Schneeflocke.lock:
            Schneeflocke.stop += 1


