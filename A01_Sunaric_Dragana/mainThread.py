import os
import threading

from Schneeflocke import Schneeflocke
import random
import time

def print_pos(threads):
    screenwidth = 200
    screenheigth = Schneeflocke.max_y
    pos = []
    for t in threads:
        pos.append((t.possition_x,t.possition_y))
    out = ""
    for h in range(0, screenheigth):
        for w in range(0, screenwidth):
            if (w, h) in pos:
                out += "❄️"
            else:
                out += " "
        out += "\n"
    print(out)


if __name__ == '__main__':
    # 10 Instanzen der Thread -Klasse erstellen
    anzSchneeflocken = 10
    threads = []
    event = threading.Event()
    for i in range (0, anzSchneeflocken):
        thread = Schneeflocke(random.randint(1,200),event)
        threads += [thread]
        thread.start ()
    print("warte auf event ....")
    time.sleep(1) #sleep für eine sekunde
    event.set() #event auslösen
    print("event wurde ausgelöst")
    # Auf die Kind -Threads warten
    while Schneeflocke.stop < anzSchneeflocken:
        time.sleep(0.5)
        for t in threads:
            print_pos(threads)







