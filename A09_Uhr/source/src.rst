src Package
===========

:mod:`xy` Module
----------------

.. automodule:: src.xy
    :members:
    :undoc-members:
    :special-members: __init__
    :show-inheritance:

